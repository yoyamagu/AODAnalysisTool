# AODAnalysisTool
## setup and compile
------------------------------------

original repository: gitlab.cern.ch/ktaniguc/AODAnalysisTool.git

- default athena version : Athena,22.0.15
- From my experience, AOD files derived from Athena,21.X.XX as input may be available without change Athena version in setup.sh
```sh
$ git clone https://gitlab.cern.ch/yoyamagu/AODAnalysisTool.git
$ cd AODAnalysisTool
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  - lxplus
#alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh' - lxplus
$ source setup.sh
$ ./compile.sh clean
```

## run
------------------------------------
```sh
$ cd run/
```
- please change the CalcEffAlg_optionsMT.py before you run
- If you just want to make Ntuple for InsideOut validation, TapMethod have nothing to do with
- outputfile name of Ntuple for InsideOut validation is automaticaly added "..._Ntuple.root"
```sh
$ LocalInputFileList="the file list name with PATH as input"
$ ......
$ job += CalcEffAlg( ....
$                    OutputFile = "the file name of output with existing PATH"
$                    ....
$                   )
```
- if you want to analyse the AOD file made by Athena 21.X.XX, please change "run.sh"
```sh
$ # bsub -q $BSUB_QUE -o ./LOGS/log_${DATE}.txt athena.py CalcEffAlg_optionsMT.py
$ bsub -q $BSUB_QUE -o ./LOGS/log_${DATE}.txt athena.py CalcEffAlg_options.py 
```
```sh
$ ./run.sh // finished running, the file for InsideOut validation is made with the name "(named by you)_Ntuple.root"
```

## Branch names
------------------------------------
| branch name | description |
|:------------:|:------------:|
| n_trig | number of trigger chains to measure |
| trigname | names of trigger chains |
| tpsumReqdRl1 | sum of dR threshold between Level1 and offline (tag + probe) |
| tpextdR | offline dR between tag and probe muons( extrapolated to Muon Spectrometer ) |
| invMass | reconstructed mass of tag and probe muons |
| tag_ReqdRL1 | dR threshold between Level1 and offline of tag muon |
| tag_pt | offline p<sub>T</sub> of tag muon |
| tag_eta | offline &eta; of tag muon |
| tag_extEta | offline &eta; of tag muon ( extrapolated to Muon Spectrometer ) |
| tag_phi | offline &phi; of tag muon |
| tag_extPhi | offline &phi; of tag muon ( extrapolated to Muon Spectrometer ) |
| tag_d0 | offline d<sub>0</sub> of tag muon |
| tag_z0 | offline z<sub>0</sub> of tag muon |
| probe_pt | offline p<sub>T</sub> of probe muon |
| probe_eta | offline &eta; of probe muon |
| probe_extEta | offline &eta; of probe muon ( extrapolated to Muon Spectrometer ) |
| probe_phi | offline &phi; of probe muon |
| probe_extPhi | offline &phi; of probe muon ( extrapolated to Muon Spectrometer ) |
| probe_d0 | offline d<sub>0</sub> of probe muon |
| probe_z0 | offline z<sub>0</sub> of probe muon |
| probe_charge | offline charge of probe muon |
| probe_segment... | offline segment. description is [here](https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODMuon/xAODMuon/versions/MuonSegment_v1.h) |

Branch names beginning with probe_L1, probe_SA, probe_CB, probe_EF are variables of probe muon measured in Level1, muonSA, muComb, EventFilter and respectively.  
For example,

### L1
| branch name | description |
|:------------:|:------------:|
| probe_L1_pass | whether pass L1 or not |
| probe_L1_eta | &eta; L1RoI |
| probe_L1_phi | &phi; L1RoI |
| probe_L1_dR | dR between probe muon and L1RoI |
| probe_L1_thrValue | The highest threshold value (in MeV) passed by the muon candidate |
| probe_L1_roiNum | RoI number |
| probe_L1_thrNumber | the logic number of the highest threshold this RoI passed |

### SA
| branch name | description |
|:------------:|:------------:|
| probe_SA_pass | whether pass muonSA or not |
| probe_SA_dR | dR between muonSA and offline muons |
| probe_SA_pt | p<sub>T</sub> measured in muonSA |
| probe_SA_eta | &eta; measured in muonSA ( in case of muonSA, etaIP ) |
| probe_SA_phi | &phi; measured in muonSA ( in case of muonSA, etaIP ) |
| probe_SA_etams | &eta; measured in muonSA ( etaMS ) |
| probe_SA_phims | &phi; measured in muonSA ( phiMS ) |
| probe_SA_etabe | &eta; measured in muonSA ( &eta; back-extrapolated to IP with same method of muComb ) |
| probe_SA_phibe | &phi; measured in muonSA ( &phi; back-extrapolated to IP with same method of muComb ) |
| probe_SA_sAddress | the station address of the muon (-1:Endcap, other:Barrel) |
| probe_SA_isRpcFailure | flag to see if RPC is properly read (if false, then falied to fit RPCs) |
| probe_SA_isTgcFailure | flag to see if TGC is properly read |
| probe_SA_superPointR_XX | the measured radious of the muon in one particular super point |
| probe_SA_superPointZ_XX | the measured Z position of the muon in one particular super point |
| probe_SA_superPointSlope_XX | the measured slope of the muon in one particular super point |
| probe_SA_superPointIntercept_XX | the measured intercept of the muon in one particular super point |
| probe_SA_superPointChi2_XX | the chi2 of the fit in one particular super point |
| probe_SA_rpcHitMeasPhi | true = &phi; strip, false = z strip |
| probe_SA_rpcHitLayer | (0,1 = low-pT plane), (2,3 = pivot plane), (4,5 = high-pT plane), (6,7 = feet's outermost plane) |
| probe_SA_mdtHitIsOutlier | 0 = used to make SuperPoint, 1 = not used |
| probe_SA_roadAw | road slope (Z/R), index (0, 1, 2) = (inner, middle, outer) |
| probe_SA_roadBw | road offset (R) |
| probe_SA_zMin | mdt region minimum z, maybe not used in Run3 (Roi based instead) |

## How to add the Ntuple contents
------------------------------------
- update soon
- add valiables in CalcEfficiency/??Objects.h, src/TrigMatchingTool.cxx, EventTree
### in SAObjects.h
```sh
$  public :
$    SAObject() :
$    isPassed(false),
$    pt(0), 
$    eta(0),
$    phi(0),
$    roiNum(-1),
$    MyNewValue(initial value)   //add!
$    {};
$    ~SAObject() {};
$  public :
$    bool isPassed;
$    double pt; 
$    double eta;
$    double phi;
$    int roiNum;
$    int MyNewValue;      //add!
```
### in TrigMatchingToolMT ( MT analysis )
```sh
$ bool TrigMatchingToolMT::matchSA( std::string& trig,
$                                   const L1Object l1obj,
$                                   SAObject& saobj )
$ {  .......
$       saobj.pt = (*l2sa)->pt();
$       saobj.eta = (*l2sa)->eta();
$       saobj.phi = (*l2sa)->phi();
$       saobj.MyNewValue = (*l2sa)->MyNewValue();    //add!
```
### in TrigMatchingTool ( Run2 analysis )
- if you want to analyse only MT file, you do not need to change here(then the value of "probe_SA_MyNewValue" in the Ntuple of output is always initial one)
- for setProbes function, matchXX get the TrigCombination variable. On the other hand, for doProbeMatching, matchXX get the FeatureContainer. Therefore, it's ok to add the new value only in matchXX for doProbeMatching
```sh
$ bool TrigMatchingTool::matchSA( const Trig::FeatureContainer& fc,
$                                 std::string& mesSATEName,
$                                 const L1Object l1obj,
$                                 SAObject& saobj )
$ {  .......
$       saobj.pt = l2sa->pt();
$       saobj.eta = l2sa->eta();
$       saobj.phi = l2sa->phi();
$       saobj.MyNewValue = l2sa->MyNewValue();   //add!
```
### in EventTreeMT.h
```sh
$    //
$    std::vector < bool >* probe_SA_pass;
$    std::vector < double >* probe_SA_pt;
$    std::vector < int >* probe_SA_MyNewValue;  //add!
```
### in EventTreeMT.cxx
```sh
$ int EventTreeMT::initialize( TString outfile = "test.root" ) {
$   probe_SA_MyNewValue   = new std::vector < int > ();    //add!
$   //
$   m_tree->Branch( "probe_SA_pass",   &probe_SA_pass );
$   m_tree->Branch( "probe_SA_MyNewValue",   &probe_SA_MyNewValue );   //add!

$ void EventTreeMT::clear() {
$   //
$   probe_SA_pass->clear();
$   probe_SA_pt->clear();
$   probe_SA_MyNewValue->clear();    //add!

$ template<typename TAP> void EventTreeMT::filltree( TAP& tap )
$ {
$ // fill the variable vectors
$   int probe_n = tap.m_vL1objects_probe.size();
$       //
$       probe_SA_pass->push_back( tap.m_vSAobjects_probe[i_probe][i_trig].isPassed );
$       probe_SA_pt->push_back( tap.m_vSAobjects_probe[i_probe][i_trig].pt );
$       probe_SA_MyNewValue->push_back( tap.m_vSAobjects_probe[i_probe][i_trig].MyNewValue );  //add!
```
## Notification
### Tag-and-probe requirements
- I implement the requirements following in [ATLAS trigger support note](https://cds.cern.ch/record/2638544/files/ATL-COM-DAQ-2018-140.pdf)
- Traditional CalcEffTool was not following this requirements but [L2MuonSA's TWiki page](https://twiki.cern.ch/twiki/bin/view/Atlas/L2MuonSA#Cut_conditions_for_efficiency_ca)
- if you want to change traditional requirements, please change m_run3evtSelection's value to false in TagAndProbe(MT).h
- the bit-calculation of L1 RoiSector's reference : [L1TGCNtuple](https://twiki.cern.ch/twiki/bin/view/Main/L1TGCNtuple#sectorAddress_8_bit_information)

## To do
------------------------------------
- enrich the Ntuple contents
- use athena message service // make this available by inheriting AgsTool to MyClass?
