#ifndef LVL1ROIS_OBJ
#define LVL1ROIS_OBJ

#include <vector>
#include <stdint.h>
class L1Object
{
  public :
    L1Object() : 
    dRl1(0),
    isPassed(-1),
    eta(0),
    phi(0),
    thrValue(0),
    roiNum(-1),
    thrNumber(0),
    roiSector(-1)
    {};
    ~L1Object() {};

  public : 
    double dRl1;
    int isPassed;
    double eta;
    double phi;
    double thrValue;
    int roiNum;
    int thrNumber;
    int roiSector;
};

//-----------------------------------------------------//
//-----------------------------------------------------//
typedef std::vector<L1Object> L1Objects;
//-----------------------------------------------------//
//-----------------------------------------------------//

#endif //LVL1ROIS_OBJ
