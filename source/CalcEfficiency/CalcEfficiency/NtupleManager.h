#ifndef _TrigMuonAnalysis_NtupleManager
#define _TrigMuonAnalysis_NtupleManager

#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include "CalcEfficiency/OfflineObjects.h"
#include "CalcEfficiency/L1Objects.h"
#include "CalcEfficiency/SAObjects.h"
#include "CalcEfficiency/CBObjects.h"
#include "CalcEfficiency/EFObjects.h"
#include "CalcEfficiency/TDTObjects.h"
#include "CalcEfficiency/Utils.h"

class NtupleManager {
	
  public:
    NtupleManager();
    ~NtupleManager();

    TFile* m_file; //!
    TTree* m_tree; //!
    OfflineObjects m_muon;
    TDTObjects m_TDTobjects;
    L1Objects m_L1objects;
    SAObjects m_SAobjects;
    CBObjects m_CBobjects;
    EFObjects m_EFobjects;
  
  public:
    int initialize( std::string outfile );
    void clear();
    void filltree( int evtNum, 
                   int runNum );
    int finalize();
    
    //event info
    int eventNumber;
    int runNumber;
    //
    std::vector<std::string>* trigname;
    std::vector<bool>* isPassedTrig;
    std::vector<bool>* isPassedL1_evt;
    std::vector<bool>* isPassedSA_evt;
    std::vector<bool>* isPassedCB_evt;
    std::vector<bool>* isPassedSAIO_evt;
    std::vector<bool>* isPassedCBIO_evt;
    std::vector<bool>* isPassedEF_evt;
    std::vector<std::vector<bool>>* isPassedL1;
    std::vector<std::vector<bool>>* isPassedSA;
    std::vector<std::vector<bool>>* isPassedCB;
    std::vector<std::vector<bool>>* isPassedSAIO;
    std::vector<std::vector<bool>>* isPassedCBIO;
    std::vector<std::vector<bool>>* isPassedEF;
    std::vector<std::vector<int>>* L1RoINumber;
    std::vector<std::vector<int>>* SARoINumber;
    std::vector<std::vector<int>>* CBRoINumber;
    std::vector<std::vector<int>>* SAIORoINumber;
    std::vector<std::vector<int>>* CBIORoINumber;
    std::vector<std::vector<int>>* L1RoISector;
    std::vector<std::vector<int>>* SARoISector;
    std::vector<std::vector<int>>* CBRoISector;
    std::vector<std::vector<int>>* SAIORoISector;
    std::vector<std::vector<int>>* CBIORoISector;
    int n_trig;
    //
    std::vector< double >* muon_pt;
    std::vector< double >* muon_eta;
    std::vector< double >* muon_phi;
    std::vector< double >* muon_extEta;
    std::vector< double >* muon_extPhi;
    //
    std::vector < double >* L1_eta;
    std::vector < double >* L1_phi;
    std::vector < double >* L1_thrValue;
    std::vector < int >* L1_roiNum;
    std::vector < int >* L1_roiSector;
    std::vector < int >* L1_thrNumber;
    //
    std::vector < double >* SA_pt;
    std::vector < double >* SA_eta;
    std::vector < double >* SA_phi;
    std::vector < double >* SA_etaMS;
    std::vector < double >* SA_phiMS;
    std::vector < int >* SA_sAddress;
    std::vector < float >* SA_roiEta;
    std::vector < float >* SA_roiPhi;
    std::vector < int >* SA_roiNum;
    std::vector < int >* SA_roiSector;
    //the measured radious of the muon in one particular super point
    std::vector < double >* SA_superPointR_BI;
    std::vector < double >* SA_superPointR_BM;
    std::vector < double >* SA_superPointR_BO;
    std::vector < double >* SA_superPointR_EI;
    std::vector < double >* SA_superPointR_EM;
    std::vector < double >* SA_superPointR_EO;
    std::vector < double >* SA_superPointR_EE;
    std::vector < double >* SA_superPointR_CSC;
    std::vector < double >* SA_superPointR_BEE;
    std::vector < double >* SA_superPointR_BME;
    //the measured Z position of the muon in one particular super point
    std::vector < double >* SA_superPointZ_BI;
    std::vector < double >* SA_superPointZ_BM;
    std::vector < double >* SA_superPointZ_BO;
    std::vector < double >* SA_superPointZ_EI;
    std::vector < double >* SA_superPointZ_EM;
    std::vector < double >* SA_superPointZ_EO;
    std::vector < double >* SA_superPointZ_EE;
    std::vector < double >* SA_superPointZ_CSC;
    std::vector < double >* SA_superPointZ_BEE;
    std::vector < double >* SA_superPointZ_BME;
    //
    std::vector < double >* CB_pt;
    std::vector < double >* CB_eta;
    std::vector < double >* CB_phi;
    std::vector < int >* CB_roiNumber;
    std::vector < int >* CB_roiSector;
    //
    std::vector < double >* EF_pt;
    std::vector < double >* EF_eta;
    std::vector < double >* EF_phi;

};

#endif

