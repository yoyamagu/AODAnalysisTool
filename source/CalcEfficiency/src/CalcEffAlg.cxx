// Gaudi
#include "GaudiKernel/IAlgTool.h"

#include "GaudiKernel/ITHistSvc.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include <iostream>
#include <fstream>
#include <vector>

// xAOD 
#include "EventInfo/EventInfo.h"
#include <EventInfo/EventID.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODTrigMuon/L2StandAloneMuon.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuon.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "GoodRunsLists/IGoodRunsListSelectorTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "TrkParameters/TrackParameters.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
#include "TrkVertexFitterInterfaces/IVertexFitter.h"

#include "TrigConfHLTData/HLTTriggerElement.h"

#include "TrigConfHLTData/HLTUtils.h"
#include "TrigConfHLTData/HLTTriggerElement.h"


#include "TrigConfL1Data/TriggerThreshold.h"
#include "TrigConfL1Data/CTPConfig.h"

//TriggerDecisionTool
#include "TrigDecisionTool/ChainGroup.h"
#include "TrigDecisionTool/Feature.h"
#include "TrigDecisionTool/FeatureContainer.h"

//MyTool
#include "CalcEfficiency/CalcEffAlg.h"

using namespace SG;
using namespace std;
using namespace TrigCompositeUtils;


//  m_trigConfTool("TrigConf::xAODConfigTool/xAODConfigTool"),
///THis function is constructor.
CalcEffAlg::CalcEffAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator),
  m_configSvc( "TrigConf::TrigConfigSvc/TrigConfigSvc", name )
{
  ///(https://twiki.cern.ch/twiki/bin/view/Sandbox/WritingYourOwnAthenaAlgorithms)Notice the calls of the declareProperty("electron_Et_min_cut", m_electron_Et_min_cut = 20*GeV) in the constructor. This makes the C++ m_electron_Et_min_cut variable configurable from the Python job options. The first argument is a string containing the Python name of the variable. The second is the C++ variable with an optional equals-sign followed by a default value. Configuration from Python will be explained more later when we get to the job options.
  declareProperty( "message", m_message);
  declareProperty( "OutputFile", m_etname );
  declareProperty( "TapMethod", m_tapmethod );
  declareProperty( "Extrapolate", m_useExt );
  declareProperty( "GRL", m_useGRL );
  declareProperty( "DataType", m_dataType );
}

StatusCode CalcEffAlg::initialize() {
  ATH_MSG_INFO("initialize()");
  ATH_MSG_INFO("My message: " << m_message);
  //==============================================================
  //==  GRL Tools
  //==============================================================
  ATH_CHECK( m_grlTool.retrieve() );
  //==============================================================
  //==  Trigger Tools
  //==============================================================
  ATH_CHECK(m_configSvc.retrieve());
  ATH_CHECK(m_trigDecTool.retrieve());
  if(m_trigDecTool->getNavigationFormat() == "TrigComposite") m_run3 = true;
  ATH_MSG_INFO("TrigDecTool->getNavigationFormat() == " << m_trigDecTool->getNavigationFormat());
  m_trigDecTool->ExperimentalAndExpertMethods()->enable();

  ////==============================================================
  ////==  Tools
  ////==============================================================
  ATH_CHECK(m_extrapolator.retrieve());
  //ATH_CHECK(m_vrtfitter.retrieve());

  ////==============================================================
  ////==  MuonExtrapolatorUtils Class
  ////==============================================================
  m_ext.initialize( m_extrapolator );
  m_isFirstEvent = true; 
  //==============================================================
  //==  TagAndProbe Class
  if(m_run3){  // Run3
    m_tapMT.initialize(m_message, m_useExt, m_tapmethod, m_ext, m_trigDecTool, m_dataType);
    m_tapMT.addMesChain( "L1_MU4", "HLT_mu4_l2io_L1MU4" );
    m_tapMT.addMesChain( "L1_MU4", "HLT_mu4_L1MU4" );
    m_tapMT.addMesChain( "L1_MU10", "HLT_2mu14_l2io_L12MU10" );
    m_tapMT.addMesChain( "L1_MU10", "HLT_2mu14_L12MU10" );
    m_tapMT.addMesChain( "L1_MU6", "HLT_2mu6_l2io_L12MU6" );
    m_tapMT.addMesChain( "L1_MU6", "HLT_2mu6_L12MU6" );
    m_tapMT.addMesChain( "L1_MU6", "HLT_mu6_L1MU6" );
    m_tapMT.addMesChain( "L1_MU10", "HLT_mu14_L1MU10" );
    m_tapMT.addMesChain( "L1_MU20", "HLT_mu26_ivarmedium_L1MU20" );
    m_histsMT.initialize( "efficiency-monitoring.root", m_tapMT );
  } else {     // Run2
    m_tap.initialize(m_message, m_useExt, m_tapmethod, m_ext, m_trigDecTool, m_dataType);
    m_tap.addMesChain( "L1_MU4", "HLT_mu4" );
    m_tap.addMesChain( "L1_MU6", "HLT_mu6" );
    m_tap.addMesChain( "L1_MU10", "HLT_mu10" );
    m_tap.addMesChain( "L1_MU10", "HLT_mu14" );
    m_tap.addMesChain( "L1_MU20", "HLT_mu26_ivarmedium" );
    // please initalize HistNtuple after addMesChain finished
    m_histsMT.initialize( "efficiency-monitoring.root", m_tap );
  }

  ////==============================================================
  ////==  VrtFitterUtils Class
  ////==============================================================
  //m_vft.initialize( m_vrtfitter );

  //==============================================================
  //==  EventTree Class
  //==============================================================
  m_etMT.initialize( m_etname );
  m_ntuple.initialize( m_etname ); //change output name in NtupleManager, so don't worry throw the same name with input for m_etMT

  ATH_CHECK( m_EventInfoKey.initialize() );
  ATH_CHECK( m_MuonContainerKey.initialize() );
  ATH_CHECK( m_MuonRoIContainerKey.initialize() );
  ATH_CHECK( m_L2SAIOKey.initialize() );
  ATH_CHECK( m_L2CBIOKey.initialize() );
  
  ATH_MSG_INFO("CalcEffAlg::initialize() end");

  return StatusCode::SUCCESS;
}

StatusCode CalcEffAlg::finalize() {
  ATH_MSG_INFO("finalize()");
  if(m_run3){
    m_histsMT.finalize( m_tapMT );
  } else {
    m_histsMT.finalize( m_tap );
  }
  m_etMT.finalize();
  m_ntuple.finalize();
  return StatusCode::SUCCESS;
}

StatusCode CalcEffAlg::execute() {
  ATH_MSG_INFO("execute()");
  if(m_run3) m_tapMT.clear();
  else       m_tap.clear();

  const EventContext& ctx = getContext();
  ATH_MSG_INFO("Get event context << " << ctx );
  ///main function to do "Tag and Probe" .
  //==============================================================
  //=  Event information
  //==============================================================
  SG::ReadHandle<xAOD::EventInfo> eventInfo(m_EventInfoKey, ctx);
  uint32_t runNumber = eventInfo->runNumber();
  unsigned long long eventNumber = eventInfo->eventNumber();
  int lumiBlock = eventInfo-> lumiBlock();
  double averageInteractionsPerCrossing =  eventInfo -> averageInteractionsPerCrossing();
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("eventNumber==========#" << eventNumber << "========" );
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("Run = " << runNumber << " : Event = " << eventNumber << " : mu = " << averageInteractionsPerCrossing );
  bool isMC = true;
  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }
  // GRL
  if( !isMC && m_useGRL ){
    ATH_MSG_INFO("Skip this event via GRL");
    if(!m_grlTool->passRunLB(*eventInfo)) return StatusCode::SUCCESS; //checks the GRL and skips to next event if not passing
  } // end if not MC
  
  // do event cleaning if not MC
  if( !isMC ){
    bool isClean = true;
    if( eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ) isClean = false;
    if( eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) isClean = false;
    if( eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ) isClean = false;
    if( eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) isClean = false;

    if(!isClean) return StatusCode::SUCCESS;

  }

  // ===== retrieve offline muons and lvl1 rois
  SG::ReadHandle<xAOD::MuonContainer> muons( m_MuonContainerKey, ctx );
  if( !muons.isValid() ){
    ATH_MSG_DEBUG("No valid MuonContainer with tag : " << m_MuonContainerKey);
    return StatusCode::SUCCESS;
  }
  SG::ReadHandle<xAOD::MuonRoIContainer> rois( m_MuonRoIContainerKey, ctx );
  if( !rois.isValid() ){
    ATH_MSG_DEBUG("No valid MuonRoIContainer with tag : " << m_MuonRoIContainerKey);
    return StatusCode::SUCCESS;
  }
  SG::ReadHandle<xAOD::L2StandAloneMuonContainer> l2sa_io( m_L2SAIOKey, ctx );
  if( !l2sa_io.isValid() ){
    ATH_MSG_DEBUG("No valid L2SA with tag : " << m_L2SAIOKey);
    return StatusCode::SUCCESS;
  }
  SG::ReadHandle<xAOD::L2CombinedMuonContainer> l2cb_io( m_L2CBIOKey, ctx );
  if( !l2cb_io.isValid() ){
    ATH_MSG_DEBUG("No valid L2SA with tag : " << m_L2SAIOKey);
    return StatusCode::SUCCESS;
  }
  // ==== check run2API or run3API via trigDecTol
  if(m_trigDecTool->getNavigationFormat() == "TriggerElement"){ //Run2 TriggerElement
    if(m_isFirstEvent){
      auto cgs = m_trigDecTool->getChainGroup("HLT.*mu.*|L1_.*MU.*|HLT_noalg_L1.*MU.*");
      for( auto &trig : cgs->getListOfTriggers() ){
        auto cg = m_trigDecTool->getChainGroup(trig);
        bool isPassedCurrent = cg->isPassed();
        ATH_MSG_INFO(trig << " is passed ==>" << isPassedCurrent);
      }
    }
    m_isFirstEvent = false;
    if(!m_tap.isPassedTrigger()) {
      ATH_MSG_DEBUG("trigger not passeed");
      return StatusCode::SUCCESS;
    }
    if(!m_tap.setProbes( *muons )){
      ATH_MSG_DEBUG("could not set tag and probes");
      return StatusCode::SUCCESS;
    }
    m_tap.doProbeMatching( rois );
    m_etMT.filltree( m_tap, eventNumber, runNumber, lumiBlock, averageInteractionsPerCrossing );
    m_histsMT.FillHist( m_tap );
  
  } else { // Run3 TrigComposite

    auto cgs = m_trigDecTool->getChainGroup("HLT.*mu.*|L1_.*MU.*|HLT_noalg_L1.*MU.*");
    for( auto &trig : cgs->getListOfTriggers() ){
      auto cg = m_trigDecTool->getChainGroup(trig);
      bool isPassedCurrent = cg->isPassed( TrigDefs::eventAccepted );
      if(trig == "HLT_mu4_l2io_L1MU4"){
        ATH_MSG_INFO(trig << " is passed ===> " << isPassedCurrent);
      }
    }
    //========== for Inside-out validation ============
    m_ntuple.m_muon.clear();
    m_ntuple.m_TDTobjects.clear();
    m_ntuple.m_L1objects.clear();
    m_ntuple.m_SAobjects.clear();
    m_ntuple.m_CBobjects.clear();
    m_ntuple.m_EFobjects.clear();
    //======== offline =======
    for( const auto& muon : *muons){
      const xAOD::TrackParticle* muontrk = muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
      if(!muontrk) continue;
      std::pair< double, double > muonextEtaAndPhi = m_ext.extTrack( muontrk );
      OfflineObject muonobj;
      muonobj.pt = muon->pt();
      muonobj.eta = muon->eta();
      muonobj.phi = muon->phi();
      muonobj.extEta = muonextEtaAndPhi.first;
      muonobj.extPhi = muonextEtaAndPhi.second;
      m_ntuple.m_muon.push_back(muonobj);
    }
    //======== L1 =======
    for(const auto& roi : *rois){
      L1Object l1obj;
      ATH_MSG_INFO("... L1RoI roiNum/thrNum/eta/phi = " << roi->getRoI() << "/" << roi->getThrNumber() << "/" << roi->eta() << "/" << roi->phi());
      l1obj.eta = roi->eta();
      l1obj.phi = roi->phi();
      l1obj.thrValue = roi->thrValue();
      l1obj.roiNum = roi->getRoI();
      l1obj.thrNumber = roi->getThrNumber();
      //how to calculate the roiSector : https://twiki.cern.ch/twiki/bin/view/Main/L1TGCNtuple#sectorAddress_8_bit_information
      uint32_t roiSec = roi->getSectorAddress() >> 1;
      (roiSec >= 64 ) ? (l1obj.roiSector = roiSec & 0x3f) : (l1obj.roiSector = roiSec & 0x1f);
      m_ntuple.m_L1objects.push_back(l1obj);
    }
    //======== SA =======
    for(const auto& saio : *l2sa_io){
      SAObject saobj;
      ATH_MSG_INFO(" #L2SAIOmode roiNum/pt/etaMS/phiMS/roiEta/roiPhi = " << saio->roiNumber() << "/" << saio->pt() << "/" << saio->etaMS() << "/" << saio->phiMS() << "/" << saio->roiEta() << "/" << saio->roiPhi());
      saobj.roiNum = saio->roiNumber();
      saobj.roiSector = saio->roiSector();
      saobj.pt = saio->pt();
      saobj.eta = saio->eta();
      saobj.phi = saio->phi();
      saobj.etaMS = saio->etaMS();
      saobj.phiMS = saio->phiMS();
      saobj.sAddress = saio->sAddress();
      saobj.roiEta  = saio->roiEta();
      saobj.roiPhi  = saio->roiPhi();
      saobj.superPointR_BI  = saio->superPointR(0);
      saobj.superPointR_BM  = saio->superPointR(1);
      saobj.superPointR_BO  = saio->superPointR(2);
      saobj.superPointR_EI  = saio->superPointR(3);
      saobj.superPointR_EM  = saio->superPointR(4);
      saobj.superPointR_EO  = saio->superPointR(5);
      saobj.superPointR_EE  = saio->superPointR(6);
      saobj.superPointR_CSC = saio->superPointR(7);
      saobj.superPointR_BEE = saio->superPointR(8);
      saobj.superPointR_BME = saio->superPointR(9);
      //float superPointZ int chamber ) const;  defined in L2StandAloneMuon.h )
      saobj.superPointZ_BI  = saio->superPointZ(0);
      saobj.superPointZ_BM  = saio->superPointZ(1);
      saobj.superPointZ_BO  = saio->superPointZ(2);
      saobj.superPointZ_EI  = saio->superPointZ(3);
      saobj.superPointZ_EM  = saio->superPointZ(4);
      saobj.superPointZ_EO  = saio->superPointZ(5);
      saobj.superPointZ_EE  = saio->superPointZ(6);
      saobj.superPointZ_CSC = saio->superPointZ(7);
      saobj.superPointZ_BEE = saio->superPointZ(8);
      saobj.superPointZ_BME = saio->superPointZ(9);
      m_ntuple.m_SAobjects.push_back(saobj);
    }
    //======== CB =======
    for(const auto& cbio : *l2cb_io){
      CBObject cbobj;
      ATH_MSG_INFO("L2CBIOmode roiNum/pt/eta/phi = " << cbio->muSATrack()->roiNumber() << "/" << cbio->pt() << "/" << cbio->eta() << "/" << cbio->phi());
      cbobj.roiNum = (int)cbio->muSATrack()->roiNumber();
      cbobj.roiSector = (int)cbio->muSATrack()->roiSector();
      cbobj.pt = cbio->pt();
      cbobj.eta = cbio->eta();
      cbobj.phi = cbio->phi();
      m_ntuple.m_CBobjects.push_back(cbobj);
    }

    //========= TDT info =========
    std::vector<std::string> hlttrigname4io, l1trigname4io;    
    hlttrigname4io.push_back("HLT_mu4_l2io_L1MU4");        l1trigname4io.push_back("L1_MU4");
    hlttrigname4io.push_back("HLT_2mu6_l2io_L12MU6");      l1trigname4io.push_back("L1_2MU6");
    hlttrigname4io.push_back("HLT_2mu14_l2io_L12MU10");    l1trigname4io.push_back("L1_2MU10");
    hlttrigname4io.push_back("HLT_mu4_L1MU4");             l1trigname4io.push_back("L1_MU4");
    hlttrigname4io.push_back("HLT_2mu6_L12MU6");           l1trigname4io.push_back("L1_2MU6");
    hlttrigname4io.push_back("HLT_2mu14_L12MU10");         l1trigname4io.push_back("L1_2MU10");
    std::vector<bool> isDimuonchain = {false, true, true, false, true, true};
    for(int i_trig=0; i_trig< (int)hlttrigname4io.size(); i_trig++){
      TDTObject tdtobj;
      tdtobj.trigChainName = hlttrigname4io.at(i_trig);
      auto cg4io = m_trigDecTool->getChainGroup(hlttrigname4io.at(i_trig));
      tdtobj.isPassedChain = cg4io->isPassed( TrigDefs::eventAccepted );
      ATH_MSG_INFO(hlttrigname4io.at(i_trig) << " is Passed? ===> " << tdtobj.isPassedChain);
      // by object
      for(const auto& roi : *rois){
        bool passThr = (roi->getThrNumber() >= L1trigThr(l1trigname4io.at(i_trig)));
        if(passThr){
          tdtobj.isPassedL1.push_back(true);
          tdtobj.L1RoINumber.push_back(roi->getRoI());
          uint32_t roiSec = roi->getSectorAddress() >> 1;
          int roiSector = -999;
          (roiSec >= 64 ) ? (roiSector = roiSec & 0x3f) : (roiSector = roiSec & 0x1f);
          tdtobj.L1RoISector.push_back(roiSector);
        } else {
          tdtobj.isPassedL1.push_back(false);
        }
      }
      // by event
      if(isDimuonchain.at(i_trig)){
        int nOfpassedThr = 0;
        for(const auto& roi : *rois){
          bool passThr = (roi->getThrNumber() >= L1trigThr(l1trigname4io.at(i_trig)));
          if(passThr) nOfpassedThr++;
        }
        if(nOfpassedThr > 1) tdtobj.isPassedL1_evt = true;
      } else {
        for(const auto& roi : *rois){
          bool passThr = (roi->getThrNumber() >= L1trigThr(l1trigname4io.at(i_trig)));
          if(passThr) tdtobj.isPassedL1_evt = true;
        }
      }

      std::vector< TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer> > l2saLinks = m_trigDecTool->features<xAOD::L2StandAloneMuonContainer>( hlttrigname4io.at(i_trig), TrigDefs::includeFailedDecisions, "HLT_MuonL2SAInfo" );
      for(const TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer>& l2saLinkInfo : l2saLinks){
        if( !l2saLinkInfo.isValid() ) continue;
        const ElementLink<xAOD::L2StandAloneMuonContainer> l2sa = l2saLinkInfo.link;
        if( !l2sa.isValid() ) continue;
        if( l2saLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE ){
          tdtobj.isPassedSA_evt = true;
          tdtobj.isPassedSA.push_back(true);
          tdtobj.SARoINumber.push_back((*l2sa)->roiNumber());
          tdtobj.SARoISector.push_back((*l2sa)->roiSector());
          ATH_MSG_INFO("ActiveState ==> ACTIVE");
        } else { 
          tdtobj.isPassedSA.push_back(false);
          ATH_MSG_INFO("ActiveState ==> INACTIVE");
        }
      }
      std::vector< TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer> > l2saLinksIO = m_trigDecTool->features<xAOD::L2StandAloneMuonContainer>( hlttrigname4io.at(i_trig), TrigDefs::includeFailedDecisions, "HLT_MuonL2SAInfoIOmode" );
      for(const TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer>& l2saLinkInfo : l2saLinksIO){
        if( !l2saLinkInfo.isValid() ) continue;
        const ElementLink<xAOD::L2StandAloneMuonContainer> l2sa = l2saLinkInfo.link;
        if( !l2sa.isValid() ) continue;
        if( l2saLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE ){
          tdtobj.isPassedSAIO_evt = true;
          tdtobj.isPassedSAIO.push_back(true);
          tdtobj.SAIORoINumber.push_back((*l2sa)->roiNumber());
          tdtobj.SAIORoISector.push_back((*l2sa)->roiSector());
          ATH_MSG_INFO("ActiveState ==> ACTIVE");
        } else { 
          tdtobj.isPassedSAIO.push_back(false);
          ATH_MSG_INFO("ActiveState ==> INACTIVE");
        }
      }
      std::vector< TrigCompositeUtils::LinkInfo<xAOD::L2CombinedMuonContainer> > l2cbLinks = m_trigDecTool->features<xAOD::L2CombinedMuonContainer>( hlttrigname4io.at(i_trig), TrigDefs::includeFailedDecisions, "HLT_MuonL2CBInfo");
      //
      for(const TrigCompositeUtils::LinkInfo<xAOD::L2CombinedMuonContainer>& l2cbLinkInfo : l2cbLinks){
        if( !l2cbLinkInfo.isValid() ) continue;
        const ElementLink<xAOD::L2CombinedMuonContainer> l2cb = l2cbLinkInfo.link;
        if( !l2cb.isValid() ) continue;
        if( l2cbLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE ){
          tdtobj.isPassedCB_evt = true;
          tdtobj.isPassedCB.push_back(true);
          tdtobj.CBRoINumber.push_back((*l2cb)->muSATrack()->roiNumber());
          tdtobj.CBRoISector.push_back((*l2cb)->muSATrack()->roiSector());
          ATH_MSG_INFO("ActiveState ==> ACTIVE");
        } else {
          tdtobj.isPassedCB.push_back(false);
          ATH_MSG_INFO("ActiveState ==> INACTIVE");
        }
      }
      std::vector< TrigCompositeUtils::LinkInfo<xAOD::L2CombinedMuonContainer> > l2cbLinksIO = m_trigDecTool->features<xAOD::L2CombinedMuonContainer>( hlttrigname4io.at(i_trig), TrigDefs::includeFailedDecisions, "HLT_MuonL2CBInfoIOmode");
      //
      for(const TrigCompositeUtils::LinkInfo<xAOD::L2CombinedMuonContainer>& l2cbLinkInfo : l2cbLinksIO){
        if( !l2cbLinkInfo.isValid() ) continue;
        const ElementLink<xAOD::L2CombinedMuonContainer> l2cb = l2cbLinkInfo.link;
        if( !l2cb.isValid() ) continue;
        if( l2cbLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE ){
          tdtobj.isPassedCBIO_evt = true;
          tdtobj.isPassedCBIO.push_back(true);
          tdtobj.CBIORoINumber.push_back((*l2cb)->muSATrack()->roiNumber());
          tdtobj.CBIORoISector.push_back((*l2cb)->muSATrack()->roiSector());
          ATH_MSG_INFO("ActiveState ==> ACTIVE");
        } else {
          tdtobj.isPassedCBIO.push_back(false);
          ATH_MSG_INFO("ActiveState ==> INACTIVE");
        }
      }
      std::vector< TrigCompositeUtils::LinkInfo<xAOD::MuonContainer> > efLinks = m_trigDecTool->features<xAOD::MuonContainer>( hlttrigname4io.at(i_trig), TrigDefs::includeFailedDecisions );
      //
      for(const TrigCompositeUtils::LinkInfo<xAOD::MuonContainer>& efLinkInfo : efLinks){
        if( !efLinkInfo.isValid() ) continue;
        const ElementLink<xAOD::MuonContainer> ef = efLinkInfo.link;
        if( !ef.isValid() ) continue;
        if( efLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE ){
          tdtobj.isPassedEF_evt = true;
          tdtobj.isPassedEF.push_back(true);
          ATH_MSG_INFO("ActiveState ==> ACTIVE");
        } else {
          tdtobj.isPassedEF.push_back(false);
          ATH_MSG_INFO("ActiveState ==> INACTIVE");
        }
      }
      
      m_ntuple.m_TDTobjects.push_back(tdtobj);
    }

    m_ntuple.filltree(eventNumber, runNumber);
    //========== making Ntuple for IO end ============

    m_tapMT.checkMesChain();

    if(!m_tapMT.isPassedTrigger()) {
      ATH_MSG_DEBUG("trigger not passeed");
      return StatusCode::SUCCESS;
    }
    if(!m_tapMT.setProbes( muons, rois )){
      ATH_MSG_DEBUG("could not set tag and probes");
      return StatusCode::SUCCESS;
    }
    m_tapMT.doProbeMatching( rois );
    m_tapMT.doTagMatching( rois );
    m_etMT.filltree( m_tapMT, eventNumber, runNumber, lumiBlock, averageInteractionsPerCrossing );
    m_histsMT.FillHist( m_tapMT );

  } // TrigComposite end

  return StatusCode::SUCCESS;
}

