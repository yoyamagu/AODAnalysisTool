//Gaudi
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/StoreGate.h"

#include "TTree.h"
#include "TH1.h"
#include "TH2.h"
#include "CalcEfficiency/HistNtupleMT.h"

HistNtupleMT::HistNtupleMT() {
}

HistNtupleMT::~HistNtupleMT() {
}

template void HistNtupleMT::FillHist<TagAndProbeMT>(TagAndProbeMT& tap);
template void HistNtupleMT::FillHist<TagAndProbe>(TagAndProbe& tap);
template <typename TAP> void HistNtupleMT::FillHist( TAP& tap )
{
  int probe_n = tap.m_vL1objects_probe.size();
  for( int i_probe = 0; i_probe < probe_n; i_probe++ ) {
    double tpsumReqdRl1 = tap.m_requirements_tag[i_probe].reqdRl1 + tap.m_requirements_probe[i_probe].reqdRl1;
    if( tpsumReqdRl1 < tap.m_probe[i_probe].tpextdR ){
      m_h_probeEvents_offpt->Fill(tap.m_probe[i_probe].pt/1000.);
      m_h_probept->Fill(tap.m_probe[i_probe].pt/1000.);

      for(int i_trig = 0; i_trig < tap.m_nmesChain; i_trig++){
        m_h_trigPassEvents[i_trig]->Fill("offline", 1); //offline
        if(tap.m_vL1objects_probe[i_probe][i_trig].isPassed == 1){
          m_h_trigPassEvents[i_trig]->Fill("L1", 1); //L1
          m_h_L1pass_offpt[i_trig]->Fill(tap.m_probe[i_probe].pt/1000);
          if(tap.m_vSAobjects_probe[i_probe][i_trig].isPassed == 1){
            m_h_trigPassEvents[i_trig]->Fill("SA", 1); //SA
            m_h_SApass_offpt[i_trig]->Fill(tap.m_probe[i_probe].pt/1000);
            if(tap.m_vCBobjects_probe[i_probe][i_trig].isPassed == 1){
              m_h_trigPassEvents[i_trig]->Fill("CB", 1); //CB
              m_h_CBpass_offpt[i_trig]->Fill(tap.m_probe[i_probe].pt/1000);
              if(tap.m_vEFobjects_probe[i_probe][i_trig].isPassed == 1){
                m_h_trigPassEvents[i_trig]->Fill("EF", 1); //EF
                m_h_EFpass_offpt[i_trig]->Fill(tap.m_probe[i_probe].pt/1000);
              }
            }
          }
        }
      } //m_nmesChain loop end
    
    }
  }
}

template int HistNtupleMT::initialize<TagAndProbeMT>( std::string histname, TagAndProbeMT& tap);
template int HistNtupleMT::initialize<TagAndProbe>( std::string histname, TagAndProbe& tap);
template <typename TAP>
int HistNtupleMT::initialize( std::string histname, TAP& tap ) {
  TString etname = histname.c_str();
  m_FILE 	= new TFile( etname, "recreate" );
  // check valiables
  m_h_probept = new TH1D("m_h_probept", "probe p_{T};p_{T}^{offline}[GeV];events", 55, 0, 110);
  //
  m_h_passedisMoreCand = new TH1D("m_h_passedisMoreCand", "N_{isMoreCandflag = true}, barrel events;;events", 2, 0, 2);
  m_h_passedisMoreCand->GetXaxis()->SetBinLabel(1, "L1 roi");
  m_h_passedisMoreCand->GetXaxis()->SetBinLabel(2, "isMoreCand true");
  m_h_passedisMoreCand->SetMinimum(0);
  //efficiency
  m_h_probeEvents_offpt = new TH1D("m_h_probeEvents_offpt", "probe events;p_{T}^{offline}[GeV];events", 100, 0, 50);
  for(int i_trig = 0; i_trig < tap.m_nmesChain; i_trig++){
    //L1 pass
    m_h_L1pass_offpt.push_back(new TH1D(Form("m_h_L1pass_%s", tap.m_L1trigmesName.at(i_trig).data()), Form("%s passed events;p_{T}^{offline}[GeV];events", tap.m_L1trigmesName.at(i_trig).data()), 100, 0, 50));
    m_eff_L1pass_offpt.push_back(new TH1D(Form("m_eff_L1pass_%s", tap.m_L1trigmesName.at(i_trig).data()), Form("%s passed efficiency;p_{T}^{offline}[GeV];#epsilon", tap.m_L1trigmesName.at(i_trig).data()), 100, 0, 50));
    //SA pass
    m_h_SApass_offpt.push_back(new TH1D(Form("m_h_SApass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed events;p_{T}^{offline}[GeV];events", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    m_eff_SApass_offpt.push_back(new TH1D(Form("m_eff_SApass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed efficiency SA/L1;p_{T}^{offline}[GeV];#epsilon", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    //CB pass
    m_h_CBpass_offpt.push_back(new TH1D(Form("m_h_CBpass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed events;p_{T}^{offline}[GeV];events", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    m_eff_CBpass_offpt.push_back(new TH1D(Form("m_eff_CBpass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed efficiency CB/SA;p_{T}^{offline}[GeV];#epsilon", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    //EF pass
    m_h_EFpass_offpt.push_back(new TH1D(Form("m_h_EFpass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed events;p_{T}^{offline}[GeV];events", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    m_eff_EFpass_offpt.push_back(new TH1D(Form("m_eff_EFpass_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed efficiency EF/CB;p_{T}^{offline}[GeV];#epsilon", tap.m_HLTtrigmesName.at(i_trig).data()), 100, 0, 50));
    // each trigger step passed events
    m_h_trigPassEvents.push_back(new TH1D(Form("m_h_trigPassEvents_%s", tap.m_HLTtrigmesName.at(i_trig).data()), Form("%s passed events;trigger step;events", tap.m_HLTtrigmesName.at(i_trig).data()), 5, 0, 5));
    m_h_trigPassEvents.at(i_trig)->GetXaxis()->SetBinLabel(1, "offline");
    m_h_trigPassEvents.at(i_trig)->GetXaxis()->SetBinLabel(2, "L1");
    m_h_trigPassEvents.at(i_trig)->GetXaxis()->SetBinLabel(3, "SA");
    m_h_trigPassEvents.at(i_trig)->GetXaxis()->SetBinLabel(4, "CB");
    m_h_trigPassEvents.at(i_trig)->GetXaxis()->SetBinLabel(5, "EF");
  }
  return 0;
}

template int HistNtupleMT::finalize<TagAndProbeMT>(TagAndProbeMT& tap);
template int HistNtupleMT::finalize<TagAndProbe>(TagAndProbe& tap);
template <typename TAP> int HistNtupleMT::finalize( TAP& tap )
{
  for(int i_trig = 0; i_trig < tap.m_nmesChain; i_trig++){
    CalcEfficiency(m_h_L1pass_offpt.at(i_trig), m_h_probeEvents_offpt, m_eff_L1pass_offpt.at(i_trig));
    CalcEfficiency(m_h_SApass_offpt.at(i_trig), m_h_L1pass_offpt.at(i_trig), m_eff_SApass_offpt.at(i_trig));
    CalcEfficiency(m_h_CBpass_offpt.at(i_trig), m_h_SApass_offpt.at(i_trig), m_eff_CBpass_offpt.at(i_trig));
    CalcEfficiency(m_h_EFpass_offpt.at(i_trig), m_h_CBpass_offpt.at(i_trig), m_eff_EFpass_offpt.at(i_trig));
  }
  m_FILE->Write();
  delete m_h_passedisMoreCand;
  delete m_h_probeEvents_offpt;
  for(int i_trig = 0; i_trig < tap.m_nmesChain; i_trig++){
    delete m_h_L1pass_offpt[i_trig];
    delete m_eff_L1pass_offpt[i_trig];
    delete m_h_SApass_offpt[i_trig];
    delete m_eff_SApass_offpt[i_trig];
    delete m_h_CBpass_offpt[i_trig];
    delete m_eff_CBpass_offpt[i_trig];
    delete m_h_EFpass_offpt[i_trig];
    delete m_eff_EFpass_offpt[i_trig];
    delete m_h_trigPassEvents[i_trig];
  }

  return 0;
}

void HistNtupleMT::CalcEfficiency(TH1D* h_num, TH1D* h_den, TH1D* h_set){
  double eff_y, eff_yerr;
  unsigned int nbins = h_den->GetXaxis()->GetNbins();
  for(unsigned int ibin = 1; ibin <= nbins; ibin++){
    double denominator = h_den->GetBinContent(ibin);
    double numerator = h_num->GetBinContent(ibin);
    eff_y = (denominator != 0) ? numerator/denominator : 0;
    eff_yerr = (denominator != 0) ? sqrt((numerator*(1.-2*eff_y)+(denominator*pow(eff_y, 2)))/pow(denominator, 2)):0;
    h_set->SetBinContent(ibin, eff_y);
    h_set->SetBinError(ibin, eff_yerr);
  }//ibin loop end
}
