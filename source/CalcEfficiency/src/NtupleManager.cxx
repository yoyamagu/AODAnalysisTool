#include <iostream>
#include <fstream>
#include <vector>

#include "CalcEfficiency/NtupleManager.h"
#include "CalcEfficiency/Utils.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TLorentzVector.h"

NtupleManager::NtupleManager() {
}

NtupleManager::~NtupleManager() {
}

int NtupleManager::initialize( std::string outfile = "test.root" ) {
  std::string name4ntuple = outfile;
  int i_name = name4ntuple.find(".");
  name4ntuple.erase(i_name);
  std::string ntupname = name4ntuple+"_Ntuple.root";

  //initialize the event tree
  m_file	= new TFile( ntupname.c_str(), "recreate" );
  //m_file	= outfile;
  m_tree 	= new TTree( "ntuple", "AODVariables" );
  //m_tree    = outtree;
  //m_tree->SetDirectory( m_file );

  //--------------------------------------------------
  // VARIABLE SET UP
  //--------------------------------------------------
	
  // Event info
  eventNumber = -1;
  runNumber   = -1;
  // trigger variables
  n_trig            = 0;
  trigname          = new std::vector<std::string>();   
  isPassedTrig      = new std::vector<bool> ();
  isPassedL1_evt      = new std::vector<bool> ();
  isPassedSA_evt      = new std::vector<bool> ();
  isPassedCB_evt      = new std::vector<bool> ();
  isPassedSAIO_evt      = new std::vector<bool> ();
  isPassedCBIO_evt      = new std::vector<bool> ();
  isPassedEF_evt      = new std::vector<bool> ();
  isPassedL1      = new std::vector<std::vector<bool>> ();
  isPassedSA      = new std::vector<std::vector<bool>> ();
  isPassedCB      = new std::vector<std::vector<bool>> ();
  isPassedSAIO      = new std::vector<std::vector<bool>> ();
  isPassedCBIO      = new std::vector<std::vector<bool>> ();
  isPassedEF      = new std::vector<std::vector<bool>> ();
  L1RoINumber      = new std::vector<std::vector<int>> ();
  SARoINumber      = new std::vector<std::vector<int>> ();
  CBRoINumber      = new std::vector<std::vector<int>> ();
  SAIORoINumber      = new std::vector<std::vector<int>> ();
  CBIORoINumber      = new std::vector<std::vector<int>> ();
  L1RoISector      = new std::vector<std::vector<int>> ();
  SARoISector      = new std::vector<std::vector<int>> ();
  CBRoISector      = new std::vector<std::vector<int>> ();
  SAIORoISector      = new std::vector<std::vector<int>> ();
  CBIORoISector      = new std::vector<std::vector<int>> ();
  //
  muon_pt           = new std::vector < double > ();
  muon_eta          = new std::vector < double > ();
  muon_phi          = new std::vector < double > ();
  muon_extEta       = new std::vector < double > ();
  muon_extPhi       = new std::vector < double > ();
  //
  L1_eta   = new std::vector < double > ();
  L1_phi   = new std::vector < double > ();
  L1_roiNum   = new std::vector < int > ();
  L1_roiSector   = new std::vector < int > ();
  L1_thrValue   = new std::vector < double > ();
  L1_thrNumber   = new std::vector < int > ();
  //
  SA_pt    = new std::vector < double > ();
  SA_eta   = new std::vector < double > ();
  SA_phi   = new std::vector < double > ();
  SA_etaMS   = new std::vector < double > ();
  SA_phiMS   = new std::vector < double > ();
  SA_sAddress = new std::vector < int > ();
  SA_roiEta = new std::vector < float > ();
  SA_roiPhi = new std::vector < float > ();
  SA_roiNum = new std::vector < int > ();
  SA_roiSector = new std::vector < int > ();

  //the measured radious of the muon in one particular super point
  SA_superPointR_BI  = new std::vector < double > ();
  SA_superPointR_BM  = new std::vector < double > ();
  SA_superPointR_BO  = new std::vector < double > ();
  SA_superPointR_EI  = new std::vector < double > ();
  SA_superPointR_EM  = new std::vector < double > ();
  SA_superPointR_EO  = new std::vector < double > ();
  SA_superPointR_EE  = new std::vector < double > ();
  SA_superPointR_CSC = new std::vector < double > ();
  SA_superPointR_BEE = new std::vector < double > ();
  SA_superPointR_BME = new std::vector < double > ();
  //the measured Z position of the muon in one particular super point
  SA_superPointZ_BI  = new std::vector < double > ();
  SA_superPointZ_BM  = new std::vector < double > ();
  SA_superPointZ_BO  = new std::vector < double > ();
  SA_superPointZ_EI  = new std::vector < double > ();
  SA_superPointZ_EM  = new std::vector < double > ();
  SA_superPointZ_EO  = new std::vector < double > ();
  SA_superPointZ_EE  = new std::vector < double > ();
  SA_superPointZ_CSC = new std::vector < double > ();
  SA_superPointZ_BEE = new std::vector < double > ();
  SA_superPointZ_BME = new std::vector < double > ();
  //
  CB_pt    = new std::vector < double > ();
  CB_eta   = new std::vector < double > ();
  CB_phi   = new std::vector < double > ();
  CB_roiNumber   = new std::vector < int > ();
  CB_roiSector   = new std::vector < int > ();
  //
  EF_pt    = new std::vector < double > ();
  EF_eta   = new std::vector < double > ();
  EF_phi   = new std::vector < double > ();
    
  //--------------------------------------------------
  // BRANCH SET UP
  //--------------------------------------------------
 
  //Event info 
  m_tree->Branch( "eventNumber",       &eventNumber );
  m_tree->Branch( "runNumber",       &runNumber );
  m_tree->Branch( "trigname",       &trigname );
  m_tree->Branch( "isPassedTrig",       &isPassedTrig );
  m_tree->Branch( "isPassedL1_evt",       &isPassedL1_evt );
  m_tree->Branch( "isPassedSA_evt",       &isPassedSA_evt );
  m_tree->Branch( "isPassedCB_evt",       &isPassedCB_evt );
  m_tree->Branch( "isPassedSAIO_evt",       &isPassedSAIO_evt );
  m_tree->Branch( "isPassedCBIO_evt",       &isPassedCBIO_evt );
  m_tree->Branch( "isPassedEF_evt",       &isPassedEF_evt );
  m_tree->Branch( "isPassedL1",       &isPassedL1 );
  m_tree->Branch( "isPassedSA",       &isPassedSA );
  m_tree->Branch( "isPassedCB",       &isPassedCB );
  m_tree->Branch( "isPassedSAIO",       &isPassedSAIO );
  m_tree->Branch( "isPassedCBIO",       &isPassedCBIO );
  m_tree->Branch( "isPassedEF",       &isPassedEF );
  m_tree->Branch( "L1RoINumber",       &L1RoINumber );
  m_tree->Branch( "SARoINumber",       &SARoINumber );
  m_tree->Branch( "CBRoINumber",       &CBRoINumber );
  m_tree->Branch( "SAIORoINumber",       &SAIORoINumber );
  m_tree->Branch( "CBIORoINumber",       &CBIORoINumber );
  m_tree->Branch( "L1RoISector",       &L1RoISector );
  m_tree->Branch( "SARoISector",       &SARoISector );
  m_tree->Branch( "CBRoISector",       &CBRoISector );
  m_tree->Branch( "SAIORoISector",       &SAIORoISector );
  m_tree->Branch( "CBIORoISector",       &CBIORoISector );
  m_tree->Branch( "n_trig",       &n_trig );
  //offline muon
  m_tree->Branch( "muon_pt",     &muon_pt );
  m_tree->Branch( "muon_eta",    &muon_eta );
  m_tree->Branch( "muon_phi",    &muon_phi );
  m_tree->Branch( "muon_extEta",    &muon_extEta );
  m_tree->Branch( "muon_extPhi",    &muon_extPhi );
  //L1
  m_tree->Branch( "L1_eta",    &L1_eta );
  m_tree->Branch( "L1_phi",    &L1_phi );
  m_tree->Branch( "L1_thrValue",    &L1_thrValue );
  m_tree->Branch( "L1_roiNum",    &L1_roiNum );
  m_tree->Branch( "L1_roiSector",    &L1_roiSector );
  m_tree->Branch( "L1_thrNumber",    &L1_thrNumber );
  //SA
  m_tree->Branch( "SA_pt",     &SA_pt );
  m_tree->Branch( "SA_eta",    &SA_eta );
  m_tree->Branch( "SA_phi",    &SA_phi );
  m_tree->Branch( "SA_etaMS",    &SA_etaMS );
  m_tree->Branch( "SA_phiMS",    &SA_phiMS );
  //
  m_tree->Branch( "SA_sAddress",  &SA_sAddress );
  m_tree->Branch( "SA_roiEta",  &SA_roiEta );
  m_tree->Branch( "SA_roiPhi",  &SA_roiPhi );
  m_tree->Branch( "SA_roiNumber",  &SA_roiNum );
  m_tree->Branch( "SA_roiSector",  &SA_roiSector );
  //
  m_tree->Branch( "SA_superPointR_BI",   &SA_superPointR_BI );
  m_tree->Branch( "SA_superPointR_BM",   &SA_superPointR_BM );
  m_tree->Branch( "SA_superPointR_BO",   &SA_superPointR_BO );
  m_tree->Branch( "SA_superPointR_EI",   &SA_superPointR_EI );
  m_tree->Branch( "SA_superPointR_EM",   &SA_superPointR_EM );
  m_tree->Branch( "SA_superPointR_EO",   &SA_superPointR_EO );
  m_tree->Branch( "SA_superPointR_EE",   &SA_superPointR_EE );
  m_tree->Branch( "SA_superPointR_CSC",  &SA_superPointR_CSC );
  m_tree->Branch( "SA_superPointR_BEE",  &SA_superPointR_BEE );
  m_tree->Branch( "SA_superPointR_BME",  &SA_superPointR_BME );
  //
  m_tree->Branch( "SA_superPointZ_BI",   &SA_superPointZ_BI );
  m_tree->Branch( "SA_superPointZ_BM",   &SA_superPointZ_BM );
  m_tree->Branch( "SA_superPointZ_BO",   &SA_superPointZ_BO );
  m_tree->Branch( "SA_superPointZ_EI",   &SA_superPointZ_EI );
  m_tree->Branch( "SA_superPointZ_EM",   &SA_superPointZ_EM );
  m_tree->Branch( "SA_superPointZ_EO",   &SA_superPointZ_EO );
  m_tree->Branch( "SA_superPointZ_EE",   &SA_superPointZ_EE );
  m_tree->Branch( "SA_superPointZ_CSC",  &SA_superPointZ_CSC );
  m_tree->Branch( "SA_superPointZ_BEE",  &SA_superPointZ_BEE );
  m_tree->Branch( "SA_superPointZ_BME",  &SA_superPointZ_BME );
  // 
  //CB
  m_tree->Branch( "CB_pt",     &CB_pt );
  m_tree->Branch( "CB_eta",    &CB_eta );
  m_tree->Branch( "CB_phi",    &CB_phi );
  m_tree->Branch( "CB_roiNumber",    &CB_roiNumber );
  m_tree->Branch( "CB_roiSector",    &CB_roiSector );
  //EF
  m_tree->Branch( "EF_pt",     &EF_pt );
  m_tree->Branch( "EF_eta",    &EF_eta );
  m_tree->Branch( "EF_phi",    &EF_phi );
  return 1;
}

void NtupleManager::clear() {
  // clear the vector for branch
  muon_pt->clear();
  muon_eta->clear();
  muon_phi->clear();
  muon_extEta->clear();
  muon_extPhi->clear();
  trigname->clear();
  isPassedTrig->clear();
  isPassedL1_evt->clear();
  isPassedSA_evt->clear();
  isPassedCB_evt->clear();
  isPassedSAIO_evt->clear();
  isPassedCBIO_evt->clear();
  isPassedEF_evt->clear();
  isPassedL1->clear();
  isPassedSA->clear();
  isPassedCB->clear();
  isPassedSAIO->clear();
  isPassedCBIO->clear();
  isPassedEF->clear();
  L1RoINumber->clear();
  SARoINumber->clear();
  CBRoINumber->clear();
  SAIORoINumber->clear();
  CBIORoINumber->clear();
  L1RoISector->clear();
  SARoISector->clear();
  CBRoISector->clear();
  SAIORoISector->clear();
  CBIORoISector->clear();
  //
  L1_eta->clear();
  L1_phi->clear();
  L1_thrValue->clear();
  L1_roiNum->clear();
  L1_roiSector->clear();
  L1_thrNumber->clear();
  //
  SA_pt->clear();
  SA_eta->clear();
  SA_phi->clear();
  SA_etaMS->clear();
  SA_phiMS->clear();
  SA_sAddress->clear();
  SA_roiEta->clear();
  SA_roiPhi->clear();
  SA_roiNum->clear();
  SA_roiSector->clear();

  //the measured radious of the muon in one particular super point
  SA_superPointR_BI->clear();
  SA_superPointR_BM->clear();
  SA_superPointR_BO->clear();
  SA_superPointR_EI->clear();
  SA_superPointR_EM->clear();
  SA_superPointR_EO->clear();
  SA_superPointR_EE->clear();
  SA_superPointR_CSC->clear();
  SA_superPointR_BEE->clear();
  SA_superPointR_BME->clear();
  //the measured Z position of the muon in one particular super point
  SA_superPointZ_BI->clear();
  SA_superPointZ_BM->clear();
  SA_superPointZ_BO->clear();
  SA_superPointZ_EI->clear();
  SA_superPointZ_EM->clear();
  SA_superPointZ_EO->clear();
  SA_superPointZ_EE->clear();
  SA_superPointZ_CSC->clear();
  SA_superPointZ_BEE->clear();
  SA_superPointZ_BME->clear();
  //
  CB_pt->clear();
  CB_eta->clear();
  CB_phi->clear();
  CB_roiSector->clear();
  CB_roiNumber->clear();
  //
  EF_pt->clear();
  EF_eta->clear();
  EF_phi->clear();
}

void NtupleManager::filltree( int evtNum,
                              int runNum )
{
// fill the variable vectors
  this->clear();
  eventNumber = evtNum;
  runNumber = runNum;
  n_trig = m_TDTobjects.size();
  for( int i_tdt = 0; i_tdt < (int)m_TDTobjects.size(); i_tdt++ ) {
    trigname->push_back(m_TDTobjects[i_tdt].trigChainName);
    isPassedTrig->push_back(m_TDTobjects[i_tdt].isPassedChain);
    isPassedL1_evt->push_back(m_TDTobjects[i_tdt].isPassedL1_evt);
    isPassedSA_evt->push_back(m_TDTobjects[i_tdt].isPassedSA_evt);
    isPassedCB_evt->push_back(m_TDTobjects[i_tdt].isPassedCB_evt);
    isPassedSAIO_evt->push_back(m_TDTobjects[i_tdt].isPassedSAIO_evt);
    isPassedCBIO_evt->push_back(m_TDTobjects[i_tdt].isPassedCBIO_evt);
    isPassedEF_evt->push_back(m_TDTobjects[i_tdt].isPassedEF_evt);
    std::vector<bool> L1, SA, CB, SAIO, CBIO, EF;
    std::vector<int> L1Num, SANum, CBNum, SAIONum, CBIONum;
    std::vector<int> L1Sec, SASec, CBSec, SAIOSec, CBIOSec;
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedL1.size(); i_obj++){
      L1.push_back(m_TDTobjects.at(i_tdt).isPassedL1.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedSA.size(); i_obj++){
      SA.push_back(m_TDTobjects.at(i_tdt).isPassedSA.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedCB.size(); i_obj++){
      CB.push_back(m_TDTobjects.at(i_tdt).isPassedCB.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedSAIO.size(); i_obj++){
      SAIO.push_back(m_TDTobjects.at(i_tdt).isPassedSAIO.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedCBIO.size(); i_obj++){
      CBIO.push_back(m_TDTobjects.at(i_tdt).isPassedCBIO.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].isPassedEF.size(); i_obj++){
      EF.push_back(m_TDTobjects.at(i_tdt).isPassedEF.at(i_obj));
    }
    isPassedL1->push_back(L1);
    isPassedSA->push_back(SA);
    isPassedCB->push_back(CB);
    isPassedSAIO->push_back(SAIO);
    isPassedCBIO->push_back(CBIO);
    isPassedEF->push_back(EF);
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].L1RoINumber.size(); i_obj++){
      L1Num.push_back(m_TDTobjects.at(i_tdt).L1RoINumber.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].SARoINumber.size(); i_obj++){
      SANum.push_back(m_TDTobjects.at(i_tdt).SARoINumber.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].CBRoINumber.size(); i_obj++){
      CBNum.push_back(m_TDTobjects.at(i_tdt).CBRoINumber.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].SAIORoINumber.size(); i_obj++){
      SAIONum.push_back(m_TDTobjects.at(i_tdt).SAIORoINumber.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].CBIORoINumber.size(); i_obj++){
      CBIONum.push_back(m_TDTobjects.at(i_tdt).CBIORoINumber.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].L1RoISector.size(); i_obj++){
      L1Sec.push_back(m_TDTobjects.at(i_tdt).L1RoISector.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].SARoISector.size(); i_obj++){
      SASec.push_back(m_TDTobjects.at(i_tdt).SARoISector.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].CBRoISector.size(); i_obj++){
      CBSec.push_back(m_TDTobjects.at(i_tdt).CBRoISector.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].SAIORoISector.size(); i_obj++){
      SAIOSec.push_back(m_TDTobjects.at(i_tdt).SAIORoISector.at(i_obj));
    }
    for(int i_obj=0; i_obj<(int)m_TDTobjects[i_tdt].CBIORoISector.size(); i_obj++){
      CBIOSec.push_back(m_TDTobjects.at(i_tdt).CBIORoISector.at(i_obj));
    }
    L1RoINumber->push_back(L1Num);
    SARoINumber->push_back(SANum);
    CBRoINumber->push_back(CBNum);
    SAIORoINumber->push_back(SAIONum);
    CBIORoINumber->push_back(CBIONum);
    L1RoISector->push_back(L1Sec);
    SARoISector->push_back(SASec);
    CBRoISector->push_back(CBSec);
    SAIORoISector->push_back(SAIOSec);
    CBIORoISector->push_back(CBIOSec);
  }
  for( int i_muon = 0; i_muon < (int)m_muon.size(); i_muon++ ) {
    muon_pt->push_back(m_muon[i_muon].pt);
    muon_eta->push_back(m_muon[i_muon].eta);
    muon_phi->push_back(m_muon[i_muon].phi);
    muon_extEta->push_back(m_muon[i_muon].extEta);
    muon_extPhi->push_back(m_muon[i_muon].extPhi);
  }
  for( int i_l1 = 0; i_l1 < (int)m_L1objects.size(); i_l1++ ) {
    L1_thrValue->push_back(m_L1objects[i_l1].thrValue);
    L1_thrNumber->push_back(m_L1objects[i_l1].thrNumber);
    L1_eta->push_back(m_L1objects[i_l1].eta);
    L1_phi->push_back(m_L1objects[i_l1].phi);
    L1_roiNum->push_back(m_L1objects[i_l1].roiNum);
    L1_roiSector->push_back(m_L1objects[i_l1].roiSector);
  }
  for( int i_sa = 0; i_sa < (int)m_SAobjects.size(); i_sa++ ) {
    SA_pt->push_back(m_SAobjects[i_sa].pt);
    SA_eta->push_back(m_SAobjects[i_sa].eta);
    SA_phi->push_back(m_SAobjects[i_sa].phi);
    SA_roiEta->push_back(m_SAobjects[i_sa].roiEta);
    SA_roiPhi->push_back(m_SAobjects[i_sa].roiPhi);
    SA_etaMS->push_back(m_SAobjects[i_sa].etaMS);
    SA_phiMS->push_back(m_SAobjects[i_sa].phiMS);
    SA_roiNum->push_back(m_SAobjects[i_sa].roiNum);
    SA_roiSector->push_back(m_SAobjects[i_sa].roiSector);
    //the measured radious of the muon in one particular super point
    SA_superPointR_BI->push_back( m_SAobjects[i_sa].superPointR_BI );
    SA_superPointR_BM->push_back( m_SAobjects[i_sa].superPointR_BM );
    SA_superPointR_BO->push_back( m_SAobjects[i_sa].superPointR_BO );
    SA_superPointR_EI->push_back( m_SAobjects[i_sa].superPointR_EI );
    SA_superPointR_EM->push_back( m_SAobjects[i_sa].superPointR_EM );
    SA_superPointR_EO->push_back( m_SAobjects[i_sa].superPointR_EO );
    SA_superPointR_EE->push_back( m_SAobjects[i_sa].superPointR_EE );
    SA_superPointR_CSC->push_back( m_SAobjects[i_sa].superPointR_CSC );
    SA_superPointR_BEE->push_back( m_SAobjects[i_sa].superPointR_BEE );
    SA_superPointR_BME->push_back( m_SAobjects[i_sa].superPointR_BME );
    //the measured Z position of the muon in one particular super point
    SA_superPointZ_BI->push_back( m_SAobjects[i_sa].superPointZ_BI );
    SA_superPointZ_BM->push_back( m_SAobjects[i_sa].superPointZ_BM );
    SA_superPointZ_BO->push_back( m_SAobjects[i_sa].superPointZ_BO );
    SA_superPointZ_EI->push_back( m_SAobjects[i_sa].superPointZ_EI );
    SA_superPointZ_EM->push_back( m_SAobjects[i_sa].superPointZ_EM );
    SA_superPointZ_EO->push_back( m_SAobjects[i_sa].superPointZ_EO );
    SA_superPointZ_EE->push_back( m_SAobjects[i_sa].superPointZ_EE );
    SA_superPointZ_CSC->push_back( m_SAobjects[i_sa].superPointZ_CSC );
    SA_superPointZ_BEE->push_back( m_SAobjects[i_sa].superPointZ_BEE );
    SA_superPointZ_BME->push_back( m_SAobjects[i_sa].superPointZ_BME );
  }
  for( int i_CB = 0; i_CB < (int)m_CBobjects.size(); i_CB++ ) {
    CB_pt->push_back(m_CBobjects[i_CB].pt);
    CB_eta->push_back(m_CBobjects[i_CB].eta);
    CB_phi->push_back(m_CBobjects[i_CB].phi);
    CB_roiNumber->push_back(m_CBobjects[i_CB].roiNum);
    CB_roiSector->push_back(m_CBobjects[i_CB].roiSector);
  }
  for( int i_EF = 0; i_EF < (int)m_EFobjects.size(); i_EF++ ) {
    EF_pt->push_back(m_EFobjects[i_EF].pt);
    EF_eta->push_back(m_EFobjects[i_EF].eta);
    EF_phi->push_back(m_EFobjects[i_EF].phi);
  }
  m_tree->Fill();
}

int NtupleManager::finalize() {
  // write into file and close	
  m_file->Write();	
  delete  muon_pt;
  delete  muon_eta;
  delete  muon_phi;
  delete  muon_extEta;
  delete  muon_extPhi;
  delete  trigname;
  delete  isPassedTrig;
  delete  isPassedL1_evt;
  delete  isPassedSA_evt;
  delete  isPassedCB_evt;
  delete  isPassedSAIO_evt;
  delete  isPassedCBIO_evt;
  delete  isPassedEF_evt;
  delete  isPassedL1;
  delete  isPassedSA;
  delete  isPassedCB;
  delete  isPassedSAIO;
  delete  isPassedCBIO;
  delete  isPassedEF;
  delete  L1RoINumber;
  delete  SARoINumber;
  delete  CBRoINumber;
  delete  SAIORoINumber;
  delete  CBIORoINumber;
  delete  L1RoISector;
  delete  SARoISector;
  delete  CBRoISector;
  delete  SAIORoISector;
  delete  CBIORoISector;
  delete  L1_eta; 
  delete  L1_phi; 
  delete  L1_thrValue; 
  delete  L1_roiNum; 
  delete  L1_roiSector; 
  delete  L1_thrNumber; 
  delete  SA_pt; 
  delete  SA_eta; 
  delete  SA_phi; 
  delete  SA_etaMS; 
  delete  SA_phiMS; 
  delete  SA_sAddress;
  delete  SA_roiEta;
  delete  SA_roiPhi;
  delete  SA_roiNum;
  delete  SA_roiSector;

  //the measured radious of the muon in one particular super point
  delete  SA_superPointR_BI;
  delete  SA_superPointR_BM;
  delete  SA_superPointR_BO;
  delete  SA_superPointR_EI;
  delete  SA_superPointR_EM;
  delete  SA_superPointR_EO;
  delete  SA_superPointR_EE;
  delete  SA_superPointR_CSC;
  delete  SA_superPointR_BEE;
  delete  SA_superPointR_BME;
  //the measured Z position of the muon in one particular super point
  delete  SA_superPointZ_BI;
  delete  SA_superPointZ_BM;
  delete  SA_superPointZ_BO;
  delete  SA_superPointZ_EI;
  delete  SA_superPointZ_EM;
  delete  SA_superPointZ_EO;
  delete  SA_superPointZ_EE;
  delete  SA_superPointZ_CSC;
  delete  SA_superPointZ_BEE;
  delete  SA_superPointZ_BME;
  delete  CB_pt; 
  delete  CB_eta; 
  delete  CB_phi; 
  delete  CB_roiNumber; 
  delete  CB_roiSector; 
  delete  EF_pt; 
  delete  EF_eta; 
  delete  EF_phi;
  
  return 1;
}            
